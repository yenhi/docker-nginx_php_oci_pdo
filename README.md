# 介紹
此Docker-compose將自動下載安裝以下image container，且自動安裝相關套件
- Nginx
- php:fpm (預設安裝php7最新版本)
  - oci8
  - pdo_oci


# Nginx設定檔
Nginx conf設定檔預設於根目錄底下  `nginx/conf.d/default.conf`
若更新Nginx設定檔案，需進入Nginx容器執行以下指令以更新設定檔

```bash
$ docker exec nginx service nginx reload
```

亦可於 `nginx/conf.d` 目錄底下自行設定多個conf設定檔來同步運作多個網站


# 網站檔案
預設網站目錄為 `www/web` 目錄下index.php （ 預設為phpinfo()資訊 ）
可於 `www` 目錄底下新增多個目錄，並配合Nginx設定將多個網站指向不同Port運行


# 安裝方法
1. 首先將此專案 git clone 到電腦目錄下，並在terminal `cd` 至該目錄。
2. 執行以下指令

```bash
$ docker-compose up -d
```

3. 於網頁瀏覽器運行 http://localhost:8080 ，若看到phpinfo相關資料即代表成功。

  **預設對外Port：**
  * **Web:** 8080
  

# 移除容器方法
於 `dcoker-compose.yml` 目錄下執行以下指令

```bash
$ docker-compose down
```

若需刪除 image 映像檔案則執行以下指令：

```bash
$ docker rmi webserver_php-fpm nginx php:fpm
```